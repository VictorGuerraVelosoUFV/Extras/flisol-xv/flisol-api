# FLISoL API

### Instalando npm
```
$ sudo apt-get install npm
```

### Instando MySQL
```
$ sudo apt-get install mysql-server
```

### Instalando Loopback
```
$ npm install loopback-cli
$ cd flisol-api/
$ PATH="`pwd`/node_modules/.bin/:$PATH"
```

### Criando o MySQL
```
# mysql -u root

mysql> CREATE USER 'flisol'@'localhost' IDENTIFIED BY 'flisol-cefet';
mysql> CREATE DATABASE `flisol-db` CHARACTER SET utf8 COLLATE utf8_general_ci;
mysql> GRANT ALL PRIVILEGES ON *.* TO 'flisol'@'localhost';
```

### Executando a Migração
```
$ npm i loopback-migration-tool
$ lb-migration migrate
$ sudo mysql -u root

mysql > use flisol-db
mysql> CREATE TABLE AccountActivity( id INTEGER PRIMARY KEY AUTO_INCREMENT, accountId INTEGER NOT NULL, activityId INTEGER NOT NULL, FOREIGN KEY (accountId) REFERENCES Account (id) ON DELETE RESTRICT ON UPDATE CASCADE, FOREIGN KEY (activityId) REFERENCES Activity (id) ON DELETE RESTRICT ON UPDATE CASCADE);
mysql > ALTER TABLE Activity MODIFY description VARCHAR(4096);
```

### Executando Loopback
```
$ node .
```

### Solução de Problemas

#### Error: Cannot resolve path "compression"
O seguinte comando deve ser executado na pasta `flisol-api`.
```
$ npm install compression
```

Esse projeto foi gerado por [LoopBack](http://loopback.io).