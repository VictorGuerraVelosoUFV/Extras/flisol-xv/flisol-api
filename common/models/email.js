module.exports = function(Email) {
    Email.sendEmail = function(sub, msg, cb) {
        Email.app.models.Email.send({
            to: 'fadoa.glauss@gmail.com',
            from: 'bh.flisol@gmail.com',
            subject: sub,
            text: msg
        }, function(err, mail) {
            if(err) {
                console.log('Email sent with errors:');
                cb(err);
            } else{
                cb(null, "Success");
            }
        });
    }

    Email.remoteMethod(
        'sendEmail',
        {
            http: {path: '/sendEmail', verb: 'post', status: 201},
            description: [ "API para envio de e-mail"],
            accepts: [{arg: 'sub', type: 'string'}, {arg: 'msg', type: 'string'}],
            returns: {arg: 'Feedback', type: 'string'},
        }
    );
};